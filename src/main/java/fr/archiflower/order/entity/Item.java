package fr.archiflower.order.entity;

import jakarta.persistence.*;

import java.util.UUID;

@Entity
@Table
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "ID", unique = true)
    private UUID id;

    @Column
    private Integer quantity;

    @ManyToOne
    private Flower flower;

    public Item() {
    }

    public Item(Integer quantity, Flower flower) {
        this.quantity = quantity;
        this.flower = flower;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Flower getFlower() {
        return flower;
    }

    public void setFlower(Flower flower) {
        this.flower = flower;
    }
}
