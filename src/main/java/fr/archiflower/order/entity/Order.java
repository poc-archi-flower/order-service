package fr.archiflower.order.entity;

import jakarta.persistence.*;
import org.springframework.cglib.core.Local;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "ID", unique = true)
    private UUID id;

    @OneToMany
    private List<Item> items;

    @Column(name = "create_date")
    private LocalDateTime createDate;

    {
        createDate = LocalDateTime.now();
        items = new ArrayList<>();
    }

    public Order() {
    }

    public Order(List<Item> items) {
        this.items = items;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public LocalDateTime getcreateDate() {
        return createDate;
    }

    public void setcreateDate(LocalDateTime create_date) {
        this.createDate = create_date;
    }
}
