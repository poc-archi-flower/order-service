package fr.archiflower.order.entity;

import jakarta.persistence.*;

import java.util.Collections;
import java.util.List;
import java.util.UUID;


public class Cart {

    private UUID id;

    private List<Item> items;

    {
        items = Collections.emptyList();
    }
    public Cart() {
    }

    public Cart(List<Item> items) {
        this.items = items;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}
