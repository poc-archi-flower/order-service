package fr.archiflower.order.controller;

import fr.archiflower.order.entity.Cart;
import fr.archiflower.order.entity.Order;
import fr.archiflower.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@CrossOrigin(origins = "*")
@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;

    // Create
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/api/order")
    public Order createOrder(@RequestBody Cart cart) {
        return orderService.createOrder(cart);
    }

    // GetById
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/api/order/{id}")
    public Order getOrder(@PathVariable UUID id) {
        return orderService.getOrderById(id);
    }

    // GetAll
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/api/order/list")
    public List<Order> getOrdersList() {
        return orderService.getAllOrders();
    }
}
