package fr.archiflower.order.service;

import fr.archiflower.order.entity.Cart;
import fr.archiflower.order.entity.Flower;
import fr.archiflower.order.entity.Item;
import fr.archiflower.order.entity.Order;
import fr.archiflower.order.repository.FlowerRepository;
import fr.archiflower.order.repository.ItemRepository;
import fr.archiflower.order.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private FlowerRepository flowerRepository;

    public Order createOrder(Cart cart) {

        // init
        Order orderCreated = new Order();
        List<Item> itemsCreated = new ArrayList<>();

        // Get cart's items
        List<Item> items = cart.getItems();

        // Save flower at them current state
        for (Item item : items) {
            Flower flower = flowerRepository.save(item.getFlower());
            itemsCreated.add(new Item(item.getQuantity(), flower));
        }
        // Save flower with quantity
        itemRepository.saveAll(itemsCreated);

        // Add items to order
        orderCreated.setItems(itemsCreated);

        // Create the order
        return  orderRepository.save(orderCreated);
    }

    public Order getOrderById(UUID id) {
        return orderRepository.findById(id).orElseThrow();
    }

    public List<Order> getAllOrders() {
        return orderRepository.findAllByOrderByCreateDateDesc();
    }

}
